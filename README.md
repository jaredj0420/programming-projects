# Projects by language

- ### Python
    - [Sort list of words by their vowels](/python/README.md)  
    
- ### C++
    - [Basics of C++ OOP](/c++/diamond/README.md)
    - [Bookstore OOP](/c++/bookstore/README.md)
    - [Student Database](/c++/student/README.md)
    
- ### Java 
    - [Dungeon Mayhem / Game Recreation](java/dungeonMayhem/README.md)
    - [Simple Temperature Conversion](java/tempConversion/README.md)
    - [Simple Number Array](java/numArray/README.md)
    - [Petstore w/ MySql](java/petstore/README.md)
    
- ### HTML/CSS/JavaScript
    - [ShoppingTally](https://shoppingtally.com)
    - [My Portfolio](https://jjaskolski.com)  

- ### Android Studio / Java
    - [Simple text view and button programming](/android/androidBasics/README.md)
    - [DASH](/android/dash/README.md) 

- ### BASH
    - [Rename files without simple commands](/bash/README.md)

# Other languages, frameworks, libraries used w/o projects
- #### Swift 
- #### SED
- #### AWK
- #### PERL
- #### Node
- #### Express
- #### React
- #### PHP

# Database knowledge
- #### MYSQL
    
