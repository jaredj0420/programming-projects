//NAME - Jared Jaskolski
//SECTION - 3

class Diamond
{
public:
	Diamond(int s, char b = '#', char f = '*');

	int GetSize() const;
	int Perimeter();
	double Area();

	int Grow();
	int Shrink();

	char SetBorder(char b);
	char SetFill(char f);

	void Draw();
	void Summary();


private:
	int size;
	int perimeter;
	double area;
	char border;
	char fill;
};



