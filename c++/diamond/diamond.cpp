//NAME - Jared Jaskolski
//SECTION - 3

#include <iostream>
#include <cmath>
#include <iomanip>
#include "diamond.h"

using namespace std;

Diamond::Diamond(int s, char b, char f) //Defining the Diamond constructor
{
	if(s < 1)
		size = 1;						//controlling the outcome of the size variable
	else if(s > 39)
		size = 39;
	else
		size = s;

	border = b;							//initializing each variable to a beginning state
	fill = f;							//so there is no garbage data
	perimeter = 0;
	area = 0.0;
}

int Diamond::GetSize() const
{
	return size;						
}

int Diamond::Perimeter()
{
	perimeter = size * 4;				//calculating the perimeter of a diamond
	return perimeter;
}

double Diamond::Area()
{
	area = ((sqrt(3) / 4) * pow(size, 2.0)) * 2;    //calculating the area of a diamond
	return area;
}

int Diamond::Grow()
{
	if(size >= 39){
		size = 39;
		return size;					//controlling what happens to size when Grow() is called
	}else{
		size += 1;
		return size;
	}
}

int Diamond::Shrink()
{
	if(size <= 1){
		size = 1;						//controlling what happens to size when Shrink() is called
		return size;
	}
	else{
		size-=1;
		return size;
	}
}

char Diamond::SetBorder(char b)
{
	if(b < 33 || b > 126){
		border = '#';	
		return border;					//controlling what happens to the border if out of bounds
	}else{								//if not out of bounds set border to parameter and return border variable
		border = b;
		return border;
	}
}

char Diamond::SetFill(char f)
{
	if(f < 33 || f > 126){				//controlling the outcome of fill if out of bounds 
		fill = '*';						//if parameter is not out of bounds set var fill to parameter and return fill
		return fill;
	}else{
		fill = f;
		return fill;
	}
	
}

void Diamond::Draw()
{
	for(int i = 0; i < size; i++){				//for loop that runs size times
		for(int k = size - 1; k > i; k--){		//var k used to control each space added to form the triangle
			cout << ' ';						//every time the nested loop is run k and i get closer together 			
		}
		if (i == 0)
			cout << border << endl;				//if i is zero add a border character
		else if (i == 1)						//if i is one add two border characters
			cout << border << ' ' << border << endl;
		else{
			cout << border << ' ';
			for(int j = 1; j < i; j++){			//else set a border then fill the triangle with the var fill
				cout << fill;					//incrementing j to add more fill characters to the triangle 
				cout << ' ';					//as i grows j loops until i is reached
			}
			cout << border << endl;
		}
	}

	for(int i = size - 2; i >= 0; i--){   		//decrementing i to take for of the flipped triangle
		for(int k = 1; k < size - i; k++){  	//k increments size times minus i to increase the ammount of spaces
			cout << ' ';
		}

		if (i == 0)
			cout << border << endl;				//if i = 0 or 1 place a border 
		else if (i == 1)
			cout << border << ' ' << border << endl;
		else{
			cout << border << ' ';
			for(int j = 1; j < i; j++){
				cout << fill;					//else set a border then fill the triangle with the var fill
				cout << ' ';					//incrementing j to add more fill characters to the triangle 
			}									//as i grows j loops until i is reached
			cout << border << endl;
		}
	}

}

void Diamond::Summary()
{
	cout << "Size of diamond's side = " << size << " units." << endl;
	cout << "Perimeter of diamond = " << perimeter << " units." << endl;	//summary function that includes size, perimeter and area
	cout << fixed << showpoint << setprecision(2);							//area is set to display two places after the decimal
	cout << "Area of diamond = " << area << " units." << endl;				//call Draw() to display the triangle object selected
	cout << "Diamond looks like:" << endl;
	Draw();
}