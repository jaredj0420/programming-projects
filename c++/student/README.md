# I/O Student Data OOP
## Project Summary
- This project takes in student data from a single txt file. The txt file is then processed / sorted and outputted to a new txt file. The output file is sorted by each major and stores the students data.
## Project Details
- Time frame: 2 weeks
- Solo project
### CPP Files
| Main.cpp |  Explanation  |
| ------ | ----- |
| ![menu](../img/studentImg/menuCpp.png)| In the Main.cpp file, I have the main process setup. Here a user selects an option from a menu. The options are to import, export, display, or exit.|  
  


| Student.cpp |  Student.h |  Explanation  |
| ------ | ----- | ----- |
| [![student](../img/studentImg/studentCpp.png)](../img/studentImg/studentCpp.png) | [![student](../img/studentImg/studentH.png)](../img/studentImg/studentH.png) | The Student.cpp holds all of the data for each student. As seen in the header file, this code uses inheritance and polymorphism to increase efficiency in time and productivity. |


| StudentList.cpp |  StudentList.h |  Explanation  |
| ------ | ----- | ----- |
| [![studentList](../img/studentImg/studentListCpp.png)](../img/studentImg/studentListCpp.png) | [![studentList](../img/studentImg/studentListH.png)](../img/studentImg/studentListH.png) | The StudentList.cpp does all of the I/O operations in the program. It takes in all of the data from the Student object and uses it to display from the main menu.|

| Input.txt |  Output.txt |  Explanation  |
| ------ | ----- | ----- |
| [![studentList](../img/studentImg/input.png)](../img/studentImg/input.png) | [![studentList](../img/studentImg/output.png)](../img/studentImg/output.png) | The data is taken from the input file and processed into the output file.|