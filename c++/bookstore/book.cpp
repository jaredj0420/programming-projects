//NAME - Jared Jaskolski
//SECTION - 3

#include <iostream>
#include <cstring>
#include <cctype>
#include <iomanip>
#include "book.h"

using namespace std;

Book::Book()
{
	strcpy(title, "");
	strcpy(author, "");
	price = 0.0;
	
}

void Book::Set(const char* t, const char* a, Genre g, double p)
{
	strcpy(title, t);
	strcpy(author, a);
	type = g;
	price = p;
}

const char* Book::GetTitle() const
{
	return title;
}

const char* Book::GetAuthor() const
{
	return author;
}

double Book::GetPrice() const
{
	return price;
}

Genre Book::GetGenre() const
{
	return type;
}

void Book::Display() const
{
	cout << title << '\t' << author << '\t' << type << setprecision(2) << fixed << "\t$" << price << endl;
}