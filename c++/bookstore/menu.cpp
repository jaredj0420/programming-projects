//NAME - Jared Jaskolski
//SECTION - 3

#include <iostream>
#include <iomanip>
#include <cstring>
#include <cctype>
#include "store.h"

using namespace std;

void Add(Store& s){		//asking user for new book input
	char c;				//input gets stored and put in parameters for the NewBook Store function
	int flag = 1;

	char t[30];
	char a[20];
	Genre h;
	double p;

	cin.ignore();

	cout << "Enter a title: ";
	cin.getline(t, 30);
	cout << "Enter an author: ";
	cin.getline(a, 20);
	cout << "Enter a genre (F = FANTASY, M = MYSTERY, S = SCIFI, C = COMPUTER): ";
	do{
		cin >> c;
		if(c == 'F' || c == 'f'){
			h = FICTION;
			flag = 0;
		}else if(c == 'M' || c == 'm'){
			h = MYSTERY;
			flag = 0;
		}else if(c == 'S' || c == 's'){
			h = SCIFI;
			flag = 0;
		}else if(c == 'C' || c == 'c'){
			h = COMPUTER;
			flag = 0;
		}else{
			cout << "Invalid option for genre, please try again (F, M, S, C): ";
		}
	}while(flag != 0);

	flag = 1;

	cout << "Enter a price: ";
	do{
		cin >> p;
		if(p < 0){
			cout << "Invalid entry, please enter a value higher than zero: ";
		}else{
			flag = 0;
		}
	}while(flag != 0);

	cout << endl;
	

	s.NewBook(t, a, h, p);
}

void LookUp(Store s)
{
	char input;				//takes in a store object 
	char t[30];				//the title or author gets sent by parameters to the the SearchTitle Store function
	char a[30];

	cout << "Would you like to search by title or author (t or a): ";
	cin >> input;

	switch(input){
		case 'a':
		case 'A':
			cout << "What is the author's name: ";
			cin.ignore();
			cin.getline(a, 30);
			s.SearchAuthor(a);
			cout << endl;
			break;
		case 'T':
		case 't':
			cout << "What is the title's name: ";
			cin.ignore();
			cin.getline(t, 30);
			s.SearchTitle(t);
			cout << endl;
			break;
	}
}

void LookUpGenre(Store s)
{
	int flag = 1;			//takes in store object
	Genre h;				//holds input in temporary data 
	char c;					//sends data through parameters to the SearchGenre Store function

	cout << "Enter the genre you would like to search for (F = FANTASY, M = MYSTERY, S = SCIFI, C = COMPUTER): ";
	do{
		cin >> c;
		if(c == 'F' || c == 'f'){
			h = FICTION;
			flag = 0;
		}else if(c == 'M' || c == 'm'){
			h = MYSTERY;
			flag = 0;
		}else if(c == 'S' || c == 's'){
			h = SCIFI;
			flag = 0;
		}else if(c == 'C' || c == 'c'){
			h = COMPUTER;
			flag = 0;
		}else{
			cout << "Invalid option for genre, please try again (F, M, S, C): ";
		}
	}while(flag != 0);
	s.SearchGenre(h);
}

void SellBook(Store& s)
{
	char title[30];

	cout << "What is the title of the book you are buying: ";		//passing title into Sell Store function
	cin.ignore();
	cin.getline(title, 30);

	s.Sell(title);

}

double SetAmount(Store& s, double& p)		//storing the amount in the register by passing in the store and user input
{
	s.amount = p;
	return s.amount;
}

void Menu() {
	cout << "A:\tAdd a book to inventory" << endl;
	cout << "F:\tFind a book from inventory" << endl;
	cout << "S:\tSell a book" << endl;
	cout << "D:\tDisplay the inventory list" << endl;
	cout << "G:\tGenre Summary" << endl;
	cout << "M:\tShow this menu" << endl;
	cout << "X:\tExit the program" << endl << endl;
}



int main(){
	Store s1;

	double amount;
	char menuChoice;
	int flag = 1;
	

	cout << "Please enter the amount of money in the register: ";
	cin >> amount;
	SetAmount(s1, amount); 
	cout << endl;

	Menu();

	do{
		int insideFlag = 1;					//loop the menu until flag is zero/exit()
		char t;
		Genre h;

		cout << "Please enter your menu selection: ";
		cin >> menuChoice;

		
		while(insideFlag != 0){

			menuChoice = tolower(menuChoice);

			if(menuChoice == 'a' || menuChoice == 'f' || menuChoice == 's' || menuChoice == 'd' || menuChoice == 'g' || menuChoice == 'm' || menuChoice == 'x'){
				insideFlag = 0;
			}else{
				cout << "Option does not exist, please try again: ";
				cin >> menuChoice;
				insideFlag = 1;
			}
		}
		insideFlag = 1;

		switch(menuChoice){
			case 'a':
				Add(s1);
				break;
			case 'f':
				LookUp(s1);
				break;
			case 's':
				SellBook(s1);
				break;
			case 'd':
				s1.Display();
				break;
			case 'g':
				LookUpGenre(s1);
				break;
			case 'm':
				Menu();
				break;
			case 'x':
				s1.Exit();
				flag = 0;
				break;
		}

	}while(flag != 0);
	

return 0;
}