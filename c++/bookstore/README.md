# Creation of bookstore application
- ### Header files
    | Book.h | Store.h |
    | -------- | --------- |
    | ![book.h](../img/bookstoreImg/bookHeader.png) | ![store.h](../img/bookstoreImg/storeHeader.png) |
    
- ### CPP files
| Main.cpp |                  |                    |                  | 
| --------------------- | ------------ | ---------- | ---------- |
| ![main](../img/bookstoreImg/menuCpp1.png) | ![main](../img/bookstoreImg/menuCpp2.png) |  ![main](../img/bookstoreImg/menuCpp3.png) | ![main](../img/bookstoreImg/menuCpp4.png) |
    
    | Store.cpp |                  |                    |
    | --------------------- | ------------ | ---------- |
    | ![store](../img/bookstoreImg/storeCpp1.png) | ![store](../img/bookstoreImg/storeCpp2.png) | ![store](../img/bookstoreImg/storeCpp3.png) |
    
    | Book.cpp |
    | --------------------- |
    | ![book](../img/bookstoreImg/bookCpp.png) |
    
- ### Terminal Run
![terminal](../img/bookstoreImg/bookstore.png)
