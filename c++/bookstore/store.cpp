//NAME - Jared Jaskolski
//SECTION - 3

#include <iostream>
#include <cstring>
#include <cctype>
#include <iomanip>
#include "store.h"

using namespace std;

ostream& operator<< (ostream& s, const Book& b)
{
	s << b;
}

Store::Store()
{
	maxSize = 5;
	currentSize = 0;
	amount = 0.0;
	bookList = new Book[maxSize];		//init data with constuctor
	books = 0;
	matches = 0;
	cost = 0;
}

Store::~Store()
{
	delete [] bookList;
}

void Store::NewBook(char* t, char* a, Genre h, double &p)
{
	if (currentSize == maxSize)	{				//NewBook takes in parameters and sets a book through the data
 		Grow();									//Grow if array is full
 		if(currentSize == 5){
 			cout << "** Array being resized to 10 allocated spots." << endl << endl;
 		}else if(currentSize == 10){
 			cout << "** Array being resized to 15 allocated spots." << endl << endl;
 		}else if(currentSize == 15){
 			cout << "** Array being resized to 20 allocated spots." << endl << endl;
 		}
	}
	bookList[currentSize].Set(t, a, h, p);		//parameters for Set() set new book in DB
	currentSize++;							
	books++;
}

void Store::Display() const 
{
	for(int i = 0; i < currentSize; i++){		//displaying all books in the DB
		PrintHeader(i);							//and amount of books / cash in register
		bookList[i].Display();
	}
	cout << endl << "There are " << books << " books in the database." << endl;
	cout << setprecision(2) << fixed << "The total amount of money remaining is $" << amount << '.' << endl << endl;
}

void Store::SearchAuthor(char* a)
{	
	for(int i = 0; i < currentSize; i++){
		if(strcmp(a, bookList[i].GetAuthor()) == 0){		//compare the parameter to all authors in the book DB
			bookList[i].Display();							//if found display those matched books
			matches++;
		}
	}

	cout << endl << "There are " << matches << " matches in your search." << endl << endl; 

	
}

int Store::SearchTitle(char* t)
{
	for(int i = 0; i < maxSize; i++){				//compare the parameter to all titles in the book DB
		PrintHeader(i);								//if found display those matched books
		if(strcmp(t, bookList[i].GetTitle()) == 0){
			bookList[i].Display();
			matches++;
			return i;
		}else if(i == maxSize){
			return -1;
		}
	}

	cout << endl << "There are " << matches << " matches in your search." << endl << endl; 
}

void Store::SearchGenre(Genre g)
{
	for(int i = 0; i < currentSize; i++){		//compare parameter to all genres in the DB
		PrintHeader(i);							//if found display all matches 
		if(g == bookList[i].GetGenre()){		//add the cost of each genre in the DB and print
			bookList[i].Display();
			matches++;
			cost += bookList[i].GetPrice();
		}
	}

	cout << endl << "There are " << matches << " matches in your search." << endl;
	cout << "The total cost of this genre of collected books is $" << cost << '.'<< endl << endl;
}

void Store::Sell(char* t)
{
		for(int i = 0; i < currentSize; i++){
		if(strcmp(t, bookList[i].GetTitle()) == 0){			//if title found add to the amount when sold
			amount += bookList[i].GetPrice();
		}
	}

	if(ReturnSearchTitle(t) == -1){
		cout << "There is no such title, returning to menu." << endl << endl;
	}else{
		for (int i = ReturnSearchTitle(t) + 1; i < currentSize; i++){		//setting each index to the index before
			bookList[i - 1] = bookList[i];
		}

		currentSize--;
		books--;


		if(currentSize >= 5 && currentSize == maxSize - 5){			//shrinking the array if conditions are met
			Shrink();
			if(currentSize == 5){
				cout << "** Array being resized to 5 allocated spots." << endl << endl;
			}else if(currentSize == 10){
				cout << "** Array being resized to 10 allocated spots." << endl << endl;
			}else if(currentSize == 15){
				cout << "** Array being resized to 15 allocated spots." << endl << endl;
			}
		}

		cout << "Thank you for your purchase of " << t << ", have a nice day!" << endl << endl;
	}			
}

void Store::Exit() const  
{
	cout << "The total left in the cash register is " << amount << endl;
}

void Store::Grow()
{
   maxSize = currentSize + 5;			
   Book* newList = new Book[maxSize];		
	
   for (int i = 0; i < currentSize; i++)	
	newList[i] = bookList[i];		
					
	delete [] bookList;
   bookList = newList;		
}

void Store::Shrink()
{
	maxSize = currentSize;
	Book* newList = new Book[maxSize];
	for (int i = 0; i < currentSize; i++)	
		newList[i] = bookList[i];

	bookList = newList;
}

void Store::PrintHeader(int i) const 
{
	if(i == 0){
		cout << endl << "Title:\t\tAuthor:\t\tType:\t\tPrice:" << endl;
	}
}

int Store::ReturnSearchTitle(char* t) const
{
	for(int i = 0; i < maxSize; i++){
		if(strcmp(t, bookList[i].GetTitle()) == 0){
			return i;
		}else if(i == maxSize){
			return -1;
		}
	}
}