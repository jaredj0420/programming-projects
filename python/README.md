# Creation of sorting application
- ### Project Details
    - Focus was on I/O control in python
    - Import data from file export to another file
    - Program takes first param as input file and second as export file
    - If a word has the same vowel set, sort by ASCII
    - Use of dictionaries and lists
    - Hits all components of python except OOP

- ### Screenshots of Terminal Runs
| First word set | Run w/ first word set |
| -------- | --------- |
| ![wordSet1](img/text1.png) | ![wordSet1](img/terminal1.png) |

    | Second word set | Run w/ second word set |
    | -------- | --------- |
    | ![wordSet2](img/text2.png) | ![wordSet2](img/terminal2.png) |
    
- ### Screenshots of Code
| sort_strings.py |         |           |
| ----- | -------- | -------- |
| [![sort](img/sort1.png)](img/sort1.png) | [![sort](img/sort2.png)](img/sort2.png) | [![sort](img/sort3.png)](img/sort3.png) |
