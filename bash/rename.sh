#if destination file is found 
#then create a variable to hold new file 
#use variable to create a new file

#else if file is not found
#create a new file with that name 

if [[ -f $2 ]] 		#if 2nd param file found
then
	for TRACKER in {0..10}	#run a loop and keep a tracker variable
	do
		
		if [ -f $2.$TRACKER ] #if a file with an extension is found run a loop
		then
			for EXT in {0..10}
			do
				if [[ -f $2.$EXT ]]				#the loop counts how many extensions are found
				then 							#then adds to the COUNTER var
					COUNTER=$((COUNTER+1))		
					NEWEXT=$2.$((COUNTER+1))
				fi
			done
			mv $1 $NEWEXT						#once the loop has ended mv rename the new file with next extension
		fi

		if [[ -f $2  &&  ! -f $2."1" ]]	#
		then
			mv $1 $2."1"
			echo "File already exists, adding new extension..."
		fi	
	done
else 
	mv $1 $2
	echo "File changed..."
fi
