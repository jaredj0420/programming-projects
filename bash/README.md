# Rename files and duplicates
- ### Program Overview
    - To test my BASH skills I chose to not use simple commands
    - Program shows basic control structures and variables 
    - Uses command line arguments / $1 = name change, $2 = destination
    - If arg 2 is found replace with arg 1
    - If arg 2 is found but already has arg 1's name, add an extension and continue until new extension is found
    
- ### Program screenshots
| First run / Before and after file changes               |                      |
| :---------------------------------: | ---- |
| Finder Before | ![finderBefore](img/finderBefore.png) |
| Terminal Before | ![terminalBefore](img/terminalBefore.png) |
| Finder After | ![finderAfter](img/finderAfter.png) |
| Terminal After | ![terminalAfter](img/terminalAfter.png)

    | Second run / Before and after file changes               |                      |
    | :---------------------------------: | ---- |
    | Finder Before | ![finderBefore](img/finderBeforeAddition.png) |
    | Terminal Before | ![terminalBefore](img/terminalAddition.png) |
    | Finder After | ![finderAfter](img/finderAdditionAfter.png) |
    | Terminal After | ![terminalAfter](img/terminalAdditionAfter.png)
