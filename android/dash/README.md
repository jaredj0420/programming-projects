# DASH
### Project Details
- 2 month timeframe
- 3 group members
- Role: Lead programmer
### Thought process
- The group wanted an app
- Figured out solutions for food problems
- Came to a solution to solve food decisions
- The app uses user data to decide on places to eat
### App Layout
- Splash: intended to grab the users attention
- Login: use login data for user accounts
- Survey: gain user data to apply it to suggestions
- Home: navigation throughout the app
- Map: google map / finds food in the nearby area
- Search: google autocomplete search / autocomplete tool to find food
- Favorites: stores restaurant favorites 
- Randomizer: decides a restaurant from users favorites
- Restaurant Page: shows all details for the selected restaurant

    |     Login      |      Survey      |  Favorites  |  Randomizer  |
    | --------------------- | ------------ | ---------- | --------- |
    | ![login](../img/dashImg/loginNoData.png) | ![survey](../img/dashImg/beginSurvey.png) | ![favorites](../img/dashImg/favorites.png) | ![randomizer](../img/dashImg/randomizer.png) |

## Map
- Use of Google Maps SDK
- Data is gathered from Google Places
- When a point of interest is clicked, the app will take you to the Restaurant page
- The data is passed with Intents
- The example image uses basic restaurant info but can access more info if needed

    |     Map      |      Map w/ Restaurant Page      |    Map code (Google Maps SDK)   |
    | ------------ | ------------ | -------- |
    | ![map](../img/dashImg/map.png) | ![survey](../img/dashImg/mapData.png) | ![survey](../img/dashImg/mapCode.png) |

## Search
- Use of Google Autocomplete SDK
- Data is gathered the same way as the map
- The only difference is the support fragments are different
- Both search and map take in data using placeFields

    |     Search      |      Search w/ Restaurant Page      |    Search code (Google Places SDK)  |
    | ------------ | ------------ | -------- |
    | ![search](../img/dashImg/searchAfter.png) | ![searchData](../img/dashImg/searchData.png) | ![searchCode](../img/dashImg/searchCode.png) |
    
## Restaurant
- Data is retrieved from intents
- With that data, I was able to set the text in the Views of the app

    |     Restaurant Page      |      Restaurant Code      |
    | ------------ | ------------ |
    | ![map](../img/dashImg/searchData.png) | ![survey](../img/dashImg/resCode.png) |