# Temperature Conversion Program
- ### Program Overview
    - Working with conversion math and control structures
    - Do while keeps user in loop until correct input
    - Basic java knowledge

- ### Program screenshots 
| Methods.java |
|     :--------:     |
| ![methods](../img/temp.png) |
    
    | Terminal Run |
    |     :--------:     |
    | ![terminal](../img/tempTerminal.png) |
