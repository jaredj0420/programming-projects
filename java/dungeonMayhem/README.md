# Dungeon Mayhem

### Project Summary
- As a senior project, I was assigned the task of recreating a board / card game. The project needed to have a GUI. 

### Project Details
- 2 month timeframe
- Solo
- Use of Java and JavaFx

### Game Rules / Details
- 4 characters to play as
- each player chooses a character
- a player is given 3 cards from the characters deck
- each card has 5 attributes respectively (attack, shield, playAgain, draw, heal)
	- attack: gives the player the ability to attack another player
	- shield: adds a shield to the player
	- playAgain: lets the player continue their turn until their turns run out
	- draw: draws specified amount of cards
	- heal: heals the player
- once a player selects a card they can use it
- the attack box will open for the player to choose who they want to attack
- game continues until there is a winner

### Game Algorithm / Explanation
- each character has their own decks
- the decks are loaded into an array when the player chooses their characters
- the algo is based off of JavaFx ids
- each card in the program has the id of characterCard[3..4..5 etc]; EX: azzanCard5
- I take the substring of the id which gives me the card I am currently using
- with this data I am able to keep track of what card needs to be pulled next

### Files w/ Code
- #### Main Functionality
	- [MainGameControl](docs/MainGameControl.java)
	- [Player](docs/Player.java)
	- [Character Selection](docs/characterSelection.java)
- #### Character Card Decks
	- [AzzanCardDeck](docs/AzzanCardDeck.java)
	- [LiaCardDeck](docs/LiaCardDeck.java)
	- [SuthaCardDeck](docs/SuthaCardDeck.java)
	- [OriaxCardDeck](docs/OriaxCardDeck.java)
- #### Character Classes
	- [liaPaladin](docs/liaPaladin.java)
	- [oriaxRogue](docs/oriaxRogue.java)
	- [suthaBarbarian](docs/suthaBarbarian.java)
	- [azzanWizard](docs/azzanWizard.java)
- #### Cards
	- [Card](docs/Card.java)

### Links of Screenshots
- #### Main Game Methods
	- [useCard()](img/use/README.md)
	- [actions()](img/action/README.md)
	- [draw()](img/draw/README.md)
	- [getSelected()](img/getSelected/README.md)
	- [selected()](img/selected/README.md)
	- [heal()](img/heal/README.md)
	- [shield()](img/shield/README.md)
