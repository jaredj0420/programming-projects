package com.example.dungeon_mayhem;

public class Player {


    Player(){
        character = "";
        hp = 10;
        shields = 0;
        actions = 1;
    }

    int getHp(){
        return hp;
    }
    int getShields(){
        return shields;
    }
    int getActions(){
        return actions;
    }

    void setCharacterChoice(int choice){
        if(choice == 1){
            liaDeck.loadDeck();
            System.out.println("Lia deck loaded");
        }else if(choice == 2){
            azzanDeck.loadDeck();
            System.out.println("Azzan deck loaded");
        }else if(choice == 3) {
            oriaxDeck.loadDeck();
            System.out.println("Oriax deck loaded");
        }else if(choice == 4) {
            suthaDeck.loadDeck();
            System.out.println("Sutha deck loaded");
        }
    }

    void setCharacterName(String name){
        character = name;
    }

    String getCharacterName() {
        return character;
    }

    oriaxRogue getOriaxCurrentCard(int id){
        return oriaxDeck.getCard(id);
    }

    suthaBarbarian getSuthaCurrentCard(int id){
        return suthaDeck.getCard(id);
    }


    //Azzan get cards
    azzanWizard getAzzanCurrentCard(int id){
        return azzanDeck.getCard(id);
    }
    int getAzzanCardNumber(){
        return azzanDeck.currentCard;
    }
    azzanWizard getAzzanDrawCards(){
        return azzanDeck.drawFromDeck();
    }
    azzanWizard[] drawAzzanFirstCards(){
        return azzanDeck.getFirstCards();
    }

    //Lia get cards
    liaPaladin[] drawLiaFirstCards(){return liaDeck.getFirstCards();}
    liaPaladin getLiaCurrentCard(int id){
        return liaDeck.getCard(id);
    }
    int getLiaCardNumber(){
        return liaDeck.currentCard;
    }
    liaPaladin getLiaDrawCards(){
        return liaDeck.drawFromDeck();
    }

    oriaxRogue[] drawOriaxFirstCards(){
        return oriaxDeck.getFirstCards();
    }

    suthaBarbarian[] drawSuthaFirstCards(){
        return suthaDeck.getFirstCards();
    }

    void showAllCards(Player p){
        if(p.getCharacterName() == "lia"){
            liaDeck.showDeck();
        }else if(p.getCharacterName() == "azzan"){
            azzanDeck.showDeck();
        }
    }

    int hp;
    int shields;
    int actions;

    LiaCardDeck liaDeck = new LiaCardDeck();
    AzzanCardDeck azzanDeck = new AzzanCardDeck();
    OriaxCardDeck oriaxDeck = new OriaxCardDeck();
    SuthaCardDeck suthaDeck = new SuthaCardDeck();
    String character;

}
