package com.example.dungeon_mayhem;

public class azzanWizard {

    azzanWizard(){
        wizardCardArray = new String[]{
                "Magic Missile",
                "Magic Missile",
                "Magic Missile",
                "Burning Hands",
                "Burning Hands",
                "Burning Hands",
                "Lightning Bolt",
                "Lightning Bolt",
                "Lightning Bolt",
                "Shield",
                "Shield",
                "Stoneskin",
                "Mirror Image",
                "Evil Sneer",
                "Evil Sneer",
                "Speed of thought",
                "Speed of thought",
                "Knowledge is Power",
                "Knowledge is Power",
                "Knowledge is Power",
                "Vampiric Touch",
                "Vampiric Touch",
                "Charm",
                "Charm",
                "Fireball",
                "Fireball"

        };
    stats = new Card();
    minStat = 1;
    maxStat = 4;
    minCard = 0;
    maxCard = wizardCardArray.length - 1;
    cardName = "";
    randNumCard = 0;
    randNumStat = 0;
    statMax = 0;
    minIcon = 0;
    maxIcon = 4;
    randNumIcon = 0;
}



    void setCardName(){
        randNumCard = (int) Math.floor(Math.random() * (maxCard - minCard + 1) + minCard);
        cardName = wizardCardArray[randNumCard];
    }

    String getCardName(){
        return cardName;
    }

    void setCardStats(){
        randNumStat = (int) Math.floor(Math.random() * (maxStat - minStat + 1) + minStat);
        stats.setCardStats(randNumStat);
    }

    void setCardIcons(){
        statMax = getCardStats();
        //loop through statMax
        for(int i = 0; i < statMax; i++){
            randNumStat = (int) Math.floor(Math.random() * (maxIcon - minIcon + 1) + minIcon);
            if(randNumStat == 0){
                stats.setAttackIcons();
            } else if(randNumStat == 1){
                stats.setHealCard();
            } else if(randNumStat == 2){
                stats.setPlayCard();
            } else if(randNumStat == 3){
                stats.setShieldIcons();
            } else if(randNumStat == 4){
                stats.setDrawCard();
            }
        }

    }

    String getCardIcons(){
        return stats.getAttackIcons() + " " + stats.getShieldIcons() + " " + stats.getPlayAgainIcons() + " " + stats.getDrawCard() + " " + stats.getHealCard();
    }

    public int getCardStats(){
        return randNumStat;
    }

    String cardName;
    int minStat;
    int maxStat;
    int minCard;
    int maxCard;
    int minIcon;
    int maxIcon;
    int randNumIcon;
    int randNumCard;
    int randNumStat;
    Card stats;
    String[] wizardCardArray;
    int statMax;

}
