package com.example.dungeon_mayhem;

public class OriaxCardDeck {
    OriaxCardDeck(){
        cards = new oriaxRogue[28];
        for(int i = 0; i < cards.length; i++){
            oriaxRogue card = new oriaxRogue();
            cards[i] = card;
        }

    }

    void loadDeck(){
        for(int i = 0; i < cards.length; i++){
            cards[i].setCardName();
            cards[i].setCardStats();
            cards[i].setCardIcons();
        }
    }

    public String showDeck(){
        return cards[0].getCardName();
    }

    public oriaxRogue[] getFirstCards(){
        return new oriaxRogue[]{cards[0], cards[1], cards[2]};

    }

    public oriaxRogue getCard(int id){
        return cards[id];
    }

    public String getCharacterName(){
        return characterName;
    }

    oriaxRogue[] cards;
    String characterName = "oriax";
}
