package com.example.dungeon_mayhem;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.stage.Stage;

import java.io.IOException;

public class HelloController {
    public Button twoPlayersBtn;
    public Button threePlayersBtn;
    public Button fourPlayersBtn;
    public Scene changeScene;
    public Stage newStage = new Stage();
    int players = 0;


    @FXML
    private Label welcomeText;

    @FXML
    public void setTwoPlayers() throws IOException {
        players = Integer.parseInt(twoPlayersBtn.getText());
        FXMLLoader fxmlLoader = new FXMLLoader(characterSelection.class.getResource("characterSelection2.fxml"));
        changeScene = new Scene(fxmlLoader.load(), 400, 400);
        newStage.setScene(changeScene);
        newStage.show();
    }

    @FXML
    public void setThreePlayers() throws IOException {
        players = Integer.parseInt(threePlayersBtn.getText());
        FXMLLoader fxmlLoader = new FXMLLoader(characterSelection.class.getResource("characterSelection2.fxml"));
        changeScene = new Scene(fxmlLoader.load(), 600, 500);
        newStage.setScene(changeScene);
        newStage.show();
    }

    @FXML
    public void setFourPlayers() throws IOException {
        players = Integer.parseInt(fourPlayersBtn.getText());
        FXMLLoader fxmlLoader = new FXMLLoader(characterSelection.class.getResource("characterSelection2.fxml"));
        changeScene = new Scene(fxmlLoader.load(), 1000, 1000);
        newStage.setScene(changeScene);
        newStage.show();
    }
}