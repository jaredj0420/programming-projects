package com.example.dungeon_mayhem;

public class suthaBarbarian {
    suthaBarbarian(){
        barbarianCardArray = new String[]{
                "Head butt",
                "Head butt",
                "Brutal Punch",
                "Brutal Punch",
                "Big Axe is Best Axe",
                "Big Axe is Best Axe",
                "Big Axe is Best Axe",
                "Big Axe is Best Axe",
                "Big Axe is Best Axe",
                "Rage!",
                "Rage!",
                "Bag of Rats",
                "Spiked Shield",
                "Riff",
                "Raff",
                "Two Axes are Better Than One",
                "Two Axes are Better Than One",
                "Open the Armory",
                "Open the Armory",
                "Snack Time",
                "Flex",
                "Flex",
                "Whirling Axes",
                "Whirling Axes",
                "Battle Roar",
                "Battle Roar",
                "Mighty Toss",
                "Mighty Toss"
        };
        stats = new Card();
        minStat = 1;
        maxStat = 4;
        minCard = 0;
        maxCard = barbarianCardArray.length - 1;
        cardName = "";
        randNumCard = 0;
        randNumStat = 0;
        statMax = 0;
        minIcon = 0;
        maxIcon = 4;
        randNumIcon = 0;
    }

    void setPaladinDeck(){

    }


    void setCardName(){
        randNumCard = (int) Math.floor(Math.random() * (maxCard - minCard + 1) + minCard);
        cardName = barbarianCardArray[randNumCard];
    }

    String getCardName(){
        return cardName;
    }

    void setCardStats(){
        randNumStat = (int) Math.floor(Math.random() * (maxStat - minStat + 1) + minStat);
        stats.setCardStats(randNumStat);
    }

    void setCardIcons(){
        statMax = getCardStats();
        //loop through statMax
        for(int i = 0; i < statMax; i++){
            randNumStat = (int) Math.floor(Math.random() * (maxIcon - minIcon + 1) + minIcon);
            if(randNumStat == 0){
                stats.setAttackIcons();
            } else if(randNumStat == 1){
                stats.setHealCard();
            } else if(randNumStat == 2){
                stats.setPlayCard();
            } else if(randNumStat == 3){
                stats.setShieldIcons();
            } else if(randNumStat == 4){
                stats.setDrawCard();
            }
        }

    }

    String getCardIcons(){
        return stats.getAttackIcons() + " " + stats.getShieldIcons() + " " + stats.getPlayAgainIcons() + " " + stats.getDrawCard() + " " + stats.getHealCard();
    }

    public int getCardStats(){
        return randNumStat;
    }

    String cardName;
    int minStat;
    int maxStat;
    int minCard;
    int maxCard;
    int minIcon;
    int maxIcon;
    int randNumIcon;
    int randNumCard;
    int randNumStat;
    Card stats;
    String[] barbarianCardArray;
    int statMax;

}

