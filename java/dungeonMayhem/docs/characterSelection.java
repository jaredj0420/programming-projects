package com.example.dungeon_mayhem;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.Pane;
import javafx.scene.shape.Ellipse;
import javafx.stage.Stage;

import java.io.IOException;


public class characterSelection {

    public Scene changeScene;
    public Stage newStage = new Stage();
    public Button doneBtn;

    @FXML Ellipse liaChoice, azzanChoice, oriaxChoice, suthaChoice;
    @FXML Pane playerPane1, playerPane2, playerPane3, playerPane4;




    public characterSelection(){
        player1 = new Player();
        player2 = new Player();
        player3 = new Player();
        player4 = new Player();
    }

    @FXML
    public void createLiaPaladin(){
        if(counter == 1){
            player1.setCharacterChoice(1);
            player1.setCharacterName("lia");
            counter++;
            liaChoice.setDisable(true);
        }else if(counter == 2){
            player2.setCharacterChoice(1);
            player2.setCharacterName("lia");
            counter++;
            liaChoice.setDisable(true);
        }else if(counter == 3){
            player3.setCharacterChoice(1);
            player3.setCharacterName("lia");
            counter++;
            liaChoice.setDisable(true);
        }else if(counter == 4){
            player4.setCharacterChoice(1);
            player4.setCharacterName("lia");
            counter++;
            liaChoice.setDisable(true);
        }

    }

    @FXML
    public void createAzzanWizard(){
        if(counter == 1){
            player1.setCharacterChoice(2);
            player1.setCharacterName("azzan");
            counter++;
            azzanChoice.setDisable(true);
        }else if(counter == 2){
            player2.setCharacterChoice(2);
            player2.setCharacterName("azzan");
            counter++;
            azzanChoice.setDisable(true);
        }else if(counter == 3){
            player3.setCharacterChoice(2);
            player3.setCharacterName("azzan");
            counter++;
            azzanChoice.setDisable(true);
        }else if(counter == 4){
            player4.setCharacterChoice(2);
            player4.setCharacterName("azzan");
            counter++;
            azzanChoice.setDisable(true);
        }
    }

    @FXML
    void createOriaxRogue(){
        if(counter == 1){
            player1.setCharacterChoice(3);
            player1.setCharacterName("oriax");
            counter++;
            oriaxChoice.setDisable(true);
        }else if(counter == 2){
            player2.setCharacterChoice(3);
            player2.setCharacterName("oriax");
            counter++;
            oriaxChoice.setDisable(true);
        }else if(counter == 3){
            player3.setCharacterChoice(3);
            player3.setCharacterName("oriax");
            counter++;
            oriaxChoice.setDisable(true);
        }else if(counter == 4){
            player4.setCharacterChoice(3);
            player4.setCharacterName("oriax");
            counter++;
            oriaxChoice.setDisable(true);
        }
    }

    @FXML
    void createSuthaBarbarian(){
        if(counter == 1){
            player1.setCharacterChoice(4);
            player1.setCharacterName("sutha");
            counter++;
            suthaChoice.setDisable(true);
        }else if(counter == 2){
            player2.setCharacterChoice(4);
            player2.setCharacterName("sutha");
            counter++;
            suthaChoice.setDisable(true);
        }else if(counter == 3){
            player3.setCharacterChoice(4);
            player3.setCharacterName("sutha");
            counter++;
            suthaChoice.setDisable(true);
        }else if(counter == 4){
            player4.setCharacterChoice(4);
            player4.setCharacterName("sutha");
            counter++;
            suthaChoice.setDisable(true);
        }
    }

    public void setChangeScene1(ActionEvent event) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(characterSelection.class.getResource("gameScreen.fxml"));
        changeScene = new Scene(fxmlLoader.load(), 1700, 600);
        MainGameControl main = fxmlLoader.getController();
        main.getInfo(player1, player2, player3, player4);
        main.setEnvironment();
        newStage.setScene(changeScene);
        newStage.show();

    }



    Player player1;
    Player player2;
    Player player3;
    Player player4;
    int counter = 1;



}
