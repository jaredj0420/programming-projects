package com.example.dungeon_mayhem;

public class oriaxRogue {
    oriaxRogue(){
        rogueCardArray = new String[]{
                "One Thrown Dagger",
                "One Thrown Dagger",
                "One Thrown Dagger",
                "One Thrown Dagger",
                "One Thrown Dagger",
                "Two Thrown Daggers",
                "Two Thrown Daggers",
                "Two Thrown Daggers",
                "Two Thrown Daggers",
                "All The Thrown Daggers",
                "All The Thrown Daggers",
                "All The Thrown Daggers",
                "Winged Serpent",
                "Winged Serpent",
                "The Goon Squad",
                "The Goon Squad",
                "My Little Friend",
                "Cunning Action",
                "Cunning Action",
                "Stolen Potion",
                "Stolen Potion",
                "Even More Daggers",
                "Sneak Attack!",
                "Sneak Attack!",
                "Pick Pocket",
                "Pick Pocket",
                "Clever Disguise",
                "Clever Disguise"
        };

        stats = new Card();
        minStat = 1;
        maxStat = 4;
        minCard = 0;
        maxCard = rogueCardArray.length - 1;
        cardName = "";
        randNumCard = 0;
        randNumStat = 0;
        statMax = 0;
        minIcon = 0;
        maxIcon = 4;
        randNumIcon = 0;
    }

    void setPaladinDeck(){

    }


    void setCardName(){
        randNumCard = (int) Math.floor(Math.random() * (maxCard - minCard + 1) + minCard);
        cardName = rogueCardArray[randNumCard];
    }

    String getCardName(){
        return cardName;
    }

    void setCardStats(){
        randNumStat = (int) Math.floor(Math.random() * (maxStat - minStat + 1) + minStat);
        stats.setCardStats(randNumStat);
    }

    void setCardIcons(){
        statMax = getCardStats();
        //loop through statMax
        for(int i = 0; i < statMax; i++){
            randNumStat = (int) Math.floor(Math.random() * (maxIcon - minIcon + 1) + minIcon);
            if(randNumStat == 0){
                stats.setAttackIcons();
            } else if(randNumStat == 1){
                stats.setHealCard();
            } else if(randNumStat == 2){
                stats.setPlayCard();
            } else if(randNumStat == 3){
                stats.setShieldIcons();
            } else if(randNumStat == 4){
                stats.setDrawCard();
            }
        }

    }

    String getCardIcons(){
        return stats.getAttackIcons() + " " + stats.getDrawCard() + " " + stats.getShieldIcons() + " " + stats.getHealCard() + " " + stats.getPlayAgainIcons();

    }

    public int getCardStats(){
        return randNumStat;
    }

    String cardName;
    int minStat;
    int maxStat;
    int minCard;
    int maxCard;
    int minIcon;
    int maxIcon;
    int randNumIcon;
    int randNumCard;
    int randNumStat;
    Card stats;
    String[] rogueCardArray;
    int statMax;

}
