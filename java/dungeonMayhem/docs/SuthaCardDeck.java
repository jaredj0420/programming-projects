package com.example.dungeon_mayhem;

public class SuthaCardDeck {
    SuthaCardDeck(){
        cards = new suthaBarbarian[28];
        for(int i = 0; i < cards.length; i++){
            suthaBarbarian card = new suthaBarbarian();
            cards[i] = card;
        }

    }

    void loadDeck(){
        for(int i = 0; i < cards.length; i++){
            cards[i].setCardName();
            cards[i].setCardStats();
            cards[i].setCardIcons();
        }
    }

    public suthaBarbarian[] getFirstCards(){
        return new suthaBarbarian[]{cards[0], cards[1], cards[2]};
    }

    public suthaBarbarian getCard(int id){
        return cards[id];
    }

    public String showDeck(){
        return cards[0].getCardName();
    }

    public String getCharacterName(){
        return characterName;
    }



    suthaBarbarian[] cards;
    String characterName = "sutha";
}
