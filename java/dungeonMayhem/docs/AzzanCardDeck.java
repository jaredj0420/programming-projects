package com.example.dungeon_mayhem;

public class AzzanCardDeck {
    AzzanCardDeck(){
        cards = new azzanWizard[28];
        for(int i = 0; i < cards.length; i++){
            azzanWizard card = new azzanWizard();
            cards[i] = card;
        }
        currentCard = 3;

    }

    void loadDeck(){
        for(int i = 0; i < cards.length; i++){
            cards[i].setCardName();
            cards[i].setCardStats();
            cards[i].setCardIcons();
        }
    }

    public String getCharacterName(){
        return characterName;
    }

    public azzanWizard[] getFirstCards(){
        return new azzanWizard[]{cards[0], cards[1], cards[2]};
    }

    public azzanWizard getCard(int id){
        return cards[id];
    }

    public azzanWizard drawFromDeck(){
        return cards[currentCard++];
    }

    public void showDeck(){
        for(int i = 0; i < cards.length; i++){
            System.out.println(i + " " + cards[i].getCardName() + " " + cards[i].getCardIcons());
        }
    }

    azzanWizard[] cards;
    String characterName = "azzan";
    int currentCard;
}

