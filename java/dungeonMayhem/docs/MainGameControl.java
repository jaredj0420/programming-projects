package com.example.dungeon_mayhem;

import javafx.animation.FadeTransition;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.effect.DropShadow;
import javafx.scene.effect.Effect;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.util.Duration;

import java.util.Objects;


public class MainGameControl {


    //card names FXML
    @FXML
    Label liaCard1Name, liaCard2Name, liaCard3Name, liaCard4Name, liaCard5Name, liaCard6Name, liaCard7Name;
    @FXML
    Label oriaxCard1Name, oriaxCard2Name, oriaxCard3Name;
    @FXML
    Label suthaCard1Name, suthaCard2Name, suthaCard3Name;
    @FXML
    Label azzanCard1Name, azzanCard2Name, azzanCard3Name, azzanCard4Name, azzanCard5Name, azzanCard6Name, azzanCard7Name;
    @FXML
    ImageView liaImg, azzanImg, oriaxImg, suthaImg;

    //liaCardLabels
    @FXML
    Label liaAttack6, liaShield6, liaAction6, liaHeal6, liaDraw6;
    @FXML
    Label liaAttack5, liaShield5, liaAction5, liaHeal5, liaDraw5;
    @FXML
    Label liaAttack4, liaShield4, liaAction4, liaHeal4, liaDraw4;
    @FXML
    Label liaAttack3, liaShield3, liaAction3, liaHeal3, liaDraw3;
    @FXML
    Label liaAttack2, liaShield2, liaAction2, liaHeal2, liaDraw2;
    @FXML
    Label liaAttack1, liaShield1, liaAction1, liaHeal1, liaDraw1;
    @FXML
    Label liaAttack0, liaShield0, liaAction0, liaHeal0, liaDraw0;
    @FXML
    Label liaHpLabel, liaShieldLabel, liaActionsLabel;

    //oriaxCardLabels
    @FXML
    Label oriaxAttack2, oriaxShield2, oriaxAction2, oriaxHeal2, oriaxDraw2;
    @FXML
    Label oriaxAttack1, oriaxShield1, oriaxAction1, oriaxHeal1, oriaxDraw1;
    @FXML
    Label oriaxAttack0, oriaxShield0, oriaxAction0, oriaxHeal0, oriaxDraw0;
    @FXML
    Label oriaxHpLabel, oriaxShieldLabel, oriaxActionsLabel;

    //azzanCardLabels
    @FXML
    Label azzanAttack6, azzanShield6, azzanAction6, azzanHeal6, azzanDraw6;
    @FXML
    Label azzanAttack5, azzanShield5, azzanAction5, azzanHeal5, azzanDraw5;
    @FXML
    Label azzanAttack4, azzanShield4, azzanAction4, azzanHeal4, azzanDraw4;
    @FXML
    Label azzanAttack3, azzanShield3, azzanAction3, azzanHeal3, azzanDraw3;
    @FXML
    Label azzanAttack2, azzanShield2, azzanAction2, azzanHeal2, azzanDraw2;
    @FXML
    Label azzanAttack1, azzanShield1, azzanAction1, azzanHeal1, azzanDraw1;
    @FXML
    Label azzanAttack0, azzanShield0, azzanAction0, azzanHeal0, azzanDraw0;
    @FXML
    Label azzanHpLabel, azzanShieldLabel, azzanActionsLabel;

    //suthaCardLabels
    @FXML
    Label suthaAttack2, suthaShield2, suthaAction2, suthaHeal2, suthaDraw2;
    @FXML
    Label suthaAttack1, suthaShield1, suthaAction1, suthaHeal1, suthaDraw1;
    @FXML
    Label suthaAttack0, suthaShield0, suthaAction0, suthaHeal0, suthaDraw0;
    @FXML
    Label suthaHpLabel, suthaShieldLabel, suthaActionsLabel;

    //all starting cards
    @FXML VBox liaCard0, liaCard1, liaCard2, liaCard3, liaCard4, liaCard5, liaCard6;
    @FXML VBox azzanCard0, azzanCard1, azzanCard2, azzanCard3, azzanCard4, azzanCard5, azzanCard6;
    @FXML VBox oriaxCard0, oriaxCard1, oriaxCard2, oriaxCard3, oriaxCard4, oriaxCard5, oriaxCard6;
    @FXML VBox suthaCard0, suthaCard1, suthaCard2, suthaCard3, suthaCard4, suthaCard5, suthaCard6;

    @FXML Pane playerPane1, playerPane2, playerPane3, playerPane4;

    @FXML Label playerTurnLabel;

    @FXML VBox attackBox;

    void getInfo(Player p1, Player p2, Player p3, Player p4){
        player1 = p1;
        player2 = p2;
        player3 = p3;
        player4 = p4;
        characters = new String[]{player1.getCharacterName(), player2.getCharacterName(), player3.getCharacterName(), player4.getCharacterName()};
        playerArray = new Player[]{player1, player2, player3, player4};

    }

    @FXML
    void changePlayerTurn(){

        if(playerTurn == 1){
            togglePlayerPanes(player1.getCharacterName());
            playerTurnLabel.setText("Player " + playerTurn + "'s Turn!");
            playerTurn = 2;
        }else if(playerTurn == 2){
            togglePlayerPanes(player2.getCharacterName());
            playerTurnLabel.setText("Player " + playerTurn + "'s Turn!");
            playerTurn = 3;
        }else if(playerTurn == 3){
            togglePlayerPanes(player3.getCharacterName());
            playerTurnLabel.setText("Player " + playerTurn + "'s Turn!");
            playerTurn = 4;
        }else if(playerTurn == 4){
            togglePlayerPanes(player4.getCharacterName());
            playerTurnLabel.setText("Player " + playerTurn + "'s Turn!");
            playerTurn = 1;
        }

    }

    void setEnvironment(){
        loadCards();
    }

    void loadCards(){
        for(int i = 0; i < characters.length; i++){
            if(characters[i] == "lia"){
                liaHpLabel.setText(Integer.toString(playerArray[i].getHp()));
                liaActionsLabel.setText(Integer.toString(playerArray[i].getActions()));
                liaShieldLabel.setText(Integer.toString(playerArray[i].getShields()));
                liaPaladin[] arr = playerArray[i].drawLiaFirstCards();

                liaCard3Name.setText(arr[2].getCardName());
                String[] iconArray = arr[2].getCardIcons().split(" ");
                liaAttack2.setText(iconArray[0]);
                liaShield2.setText(iconArray[1]);
                liaAction2.setText(iconArray[2]);
                liaDraw2.setText(iconArray[3]);
                liaHeal2.setText(iconArray[4]);

                iconArray = arr[1].getCardIcons().split(" ");
                liaCard2Name.setText(arr[1].getCardName());
                liaAttack1.setText(iconArray[0]);
                liaShield1.setText(iconArray[1]);
                liaAction1.setText(iconArray[2]);
                liaDraw1.setText(iconArray[3]);
                liaHeal1.setText(iconArray[4]);


                liaCard1Name.setText(arr[0].getCardName());
                iconArray = arr[0].getCardIcons().split(" ");
                liaAttack0.setText(iconArray[0]);
                liaShield0.setText(iconArray[1]);
                liaAction0.setText(iconArray[2]);
                liaDraw0.setText(iconArray[3]);
                liaHeal0.setText(iconArray[4]);
            }else if(characters[i] == "oriax"){
                oriaxHpLabel.setText(Integer.toString(playerArray[i].getHp()));
                oriaxActionsLabel.setText(Integer.toString(playerArray[i].getActions()));
                oriaxShieldLabel.setText(Integer.toString(playerArray[i].getShields()));
                oriaxRogue[] arr = playerArray[i].drawOriaxFirstCards();

                oriaxCard1Name.setText(arr[0].getCardName());
                String[] iconArray = arr[0].getCardIcons().split(" ");
                oriaxAttack0.setText(iconArray[0]);
                oriaxShield0.setText(iconArray[1]);
                oriaxAction0.setText(iconArray[2]);
                oriaxDraw0.setText(iconArray[3]);
                oriaxHeal0.setText(iconArray[4]);

                oriaxCard2Name.setText(arr[1].getCardName());
                iconArray = arr[1].getCardIcons().split(" ");
                oriaxAttack1.setText(iconArray[0]);
                oriaxShield1.setText(iconArray[1]);
                oriaxAction1.setText(iconArray[2]);
                oriaxDraw1.setText(iconArray[3]);
                oriaxHeal1.setText(iconArray[4]);

                oriaxCard3Name.setText(arr[2].getCardName());
                iconArray = arr[2].getCardIcons().split(" ");
                oriaxAttack2.setText(iconArray[0]);
                oriaxShield2.setText(iconArray[1]);
                oriaxAction2.setText(iconArray[2]);
                oriaxDraw2.setText(iconArray[3]);
                oriaxHeal2.setText(iconArray[4]);
            }else if(characters[i] == "azzan"){
                azzanHpLabel.setText(Integer.toString(playerArray[i].getHp()));
                azzanActionsLabel.setText(Integer.toString(playerArray[i].getActions()));
                azzanShieldLabel.setText(Integer.toString(playerArray[i].getShields()));
                azzanWizard[] arr = playerArray[i].drawAzzanFirstCards();

                azzanCard1Name.setText(arr[0].getCardName());
                String[] iconArray = arr[0].getCardIcons().split(" ");
                azzanAttack0.setText(iconArray[0]);
                azzanShield0.setText(iconArray[1]);
                azzanAction0.setText(iconArray[2]);
                azzanDraw0.setText(iconArray[3]);
                azzanHeal0.setText(iconArray[4]);

                azzanCard2Name.setText(arr[1].getCardName());
                iconArray = arr[1].getCardIcons().split(" ");
                azzanAttack1.setText(iconArray[0]);
                azzanShield1.setText(iconArray[1]);
                azzanAction1.setText(iconArray[2]);
                azzanDraw1.setText(iconArray[3]);
                azzanHeal1.setText(iconArray[4]);

                azzanCard3Name.setText(arr[2].getCardName());
                iconArray = arr[2].getCardIcons().split(" ");
                azzanAttack2.setText(iconArray[0]);
                azzanShield2.setText(iconArray[1]);
                azzanAction2.setText(iconArray[2]);
                azzanDraw2.setText(iconArray[3]);
                azzanHeal2.setText(iconArray[4]);


            }else if(characters[i] == "sutha"){
                suthaHpLabel.setText(Integer.toString(playerArray[i].getHp()));
                suthaActionsLabel.setText(Integer.toString(playerArray[i].getActions()));
                suthaShieldLabel.setText(Integer.toString(playerArray[i].getShields()));
                suthaBarbarian[] arr = playerArray[i].drawSuthaFirstCards();

                suthaCard1Name.setText(arr[0].getCardName());
                String[] iconArray = arr[0].getCardIcons().split(" ");
                suthaAttack0.setText(iconArray[0]);
                suthaShield0.setText(iconArray[1]);
                suthaAction0.setText(iconArray[2]);
                suthaDraw0.setText(iconArray[3]);
                suthaHeal0.setText(iconArray[4]);

                suthaCard2Name.setText(arr[1].getCardName());
                iconArray = arr[1].getCardIcons().split(" ");
                suthaAttack1.setText(iconArray[0]);
                suthaShield1.setText(iconArray[1]);
                suthaAction1.setText(iconArray[2]);
                suthaDraw1.setText(iconArray[3]);
                suthaHeal1.setText(iconArray[4]);

                suthaCard3Name.setText(arr[2].getCardName());
                iconArray = arr[2].getCardIcons().split(" ");
                suthaAttack2.setText(iconArray[0]);
                suthaShield2.setText(iconArray[1]);
                suthaAction2.setText(iconArray[2]);
                suthaDraw2.setText(iconArray[3]);
                suthaHeal2.setText(iconArray[4]);
            }
        }
    }

    public void selected(Event e) {
        if (toggle == 0) {
            vbox = (VBox) e.getTarget();
            id = vbox.getId();
            if(playerTurn == 1){
                getSelectedCardStats(player1, id);
            }
            vbox.setEffect(shadow);
            toggle = 1;
            previouslySelectedVbox = vbox;
        }else if(toggle == 1){
            vbox = (VBox) e.getTarget();
            if(previouslySelectedVbox == vbox){
                toggle = 1;
                cardSelected = true;
                id = vbox.getId();
                if(playerTurn-1 == 1){
                    getSelectedCardStats(player1, id);


                }if(playerTurn-1 == 2){
                    getSelectedCardStats(player2, id);
                }else if(playerTurn-1 == 3){
                    getSelectedCardStats(player3, id);
                }else if(playerTurn-1 == 0){
                    getSelectedCardStats(player4, id);
                }
            }else{
                previouslySelectedVbox.setEffect(null);
                vbox.setEffect(shadow);
                id = vbox.getId();
                if(playerTurn-1 == 1){
                    getSelectedCardStats(player1, id);
                    System.out.println(currentSelectedCardStats);
                }else if(playerTurn-1 == 2){
                    getSelectedCardStats(player2, id);
                }else if(playerTurn-1 == 3){
                    getSelectedCardStats(player3, id);
                }else if(playerTurn-1 == 0){
                    getSelectedCardStats(player4, id);
                }
                toggle = 1;
                previouslySelectedVbox = vbox;
                cardSelected = true;
            }
        }
    }

    void getSelectedCardStats(Player p, String id){
        String idSub = "";
        if(id.charAt(id.length() - 2) == 'd'){
            idSub = id.substring(id.length() - 1);
        }else{
            idSub = id.substring(id.length() - 2);
        }

        int newId = Integer.parseInt(idSub);

        if(Objects.equals(p.getCharacterName(), "lia")){
            liaPaladin card = p.getLiaCurrentCard(newId);
            currentSelectedCardStats = card.getCardIcons();
            String[] iconArray = currentSelectedCardStats.split(" ");
            currentCardAttack = Integer.parseInt(iconArray[0]);
            currentCardShields = Integer.parseInt(iconArray[1]);
            currentCardActions = Integer.parseInt(iconArray[2]);
            currentCardDraw = Integer.parseInt(iconArray[3]);
            currentCardHeal = Integer.parseInt(iconArray[4]);

        }else if(Objects.equals(p.getCharacterName(), "oriax")){
            oriaxRogue card = p.getOriaxCurrentCard(newId);
            currentSelectedCardStats = card.getCardIcons();
            String[] iconArray = currentSelectedCardStats.split(" ");
            currentCardAttack = Integer.parseInt(iconArray[0]);
            currentCardShields = Integer.parseInt(iconArray[1]);
            currentCardActions = Integer.parseInt(iconArray[2]);
            currentCardDraw = Integer.parseInt(iconArray[3]);
            currentCardHeal = Integer.parseInt(iconArray[4]);
        }else if(p.getCharacterName() == "sutha"){
            suthaBarbarian card = p.getSuthaCurrentCard(newId);
            currentSelectedCardStats = card.getCardIcons();
            String[] iconArray = currentSelectedCardStats.split(" ");
            currentCardAttack = Integer.parseInt(iconArray[0]);
            currentCardShields = Integer.parseInt(iconArray[1]);
            currentCardActions = Integer.parseInt(iconArray[2]);
            currentCardDraw = Integer.parseInt(iconArray[3]);
            currentCardHeal = Integer.parseInt(iconArray[4]);
        }else if(p.getCharacterName() == "azzan"){
            azzanWizard card = p.getAzzanCurrentCard(newId);
            currentSelectedCardStats = card.getCardIcons();
            System.out.println(currentSelectedCardStats);
            String[] iconArray = currentSelectedCardStats.split(" ");
            currentCardAttack = Integer.parseInt(iconArray[0]);
            currentCardShields = Integer.parseInt(iconArray[1]);
            currentCardActions = Integer.parseInt(iconArray[2]);
            currentCardDraw = Integer.parseInt(iconArray[3]);
            currentCardHeal = Integer.parseInt(iconArray[4]);
        }

    }


    @FXML
    void useCard(){
        liaCardArray = new VBox[]{liaCard0, liaCard1, liaCard2, liaCard3, liaCard4, liaCard5, liaCard6};
        oriaxCardArray = new VBox[]{oriaxCard0, oriaxCard1, oriaxCard2, oriaxCard3, oriaxCard4, oriaxCard5, oriaxCard6};
        azzanCardArray = new VBox[]{azzanCard0, azzanCard1, azzanCard2, azzanCard3, azzanCard4, azzanCard5, azzanCard6};
        suthaCardArray = new VBox[]{suthaCard0, suthaCard1, suthaCard2, suthaCard3, suthaCard4, suthaCard5, suthaCard6};
        if(playerTurn-1 == 1){
            ////actions////
            actions(player1, playerTurn);
            ////////attack////////
            //check local attackPlayer function
            if(currentCardAttack > 0){
                attackBox.setDisable(false);
            }
            //shields//
            shields(player1);
            draw(player1);

            if(possibleDrawTwo(player1)){
                drawTwoCards(player1);
            }

        }else if(playerTurn-1 == 2){
            actions(player2, playerTurn);
            ////////attack////////
            if(currentCardAttack > 0){
                attackBox.setDisable(false);
            }
            shields(player2);
            draw(player2);

            if(possibleDrawTwo(player2)){
                drawTwoCards(player2);
            }
        }else if(playerTurn-1 == 3){
            actions(player3, playerTurn);
            ////////attack////////
            if(currentCardAttack > 0){
                attackBox.setDisable(false);
            }
            shields(player3);
            draw(player3);

            if(possibleDrawTwo(player3)){
                drawTwoCards(player3);
            }
        }else if(playerTurn-1 == 0){
            actions(player4, playerTurn);
            ////////attack////////
            if(currentCardAttack > 0){
                attackBox.setDisable(false);
            }
            shields(player4);
            draw(player4);

            if(possibleDrawTwo(player4)){
                drawTwoCards(player4);
            }
        }
    }

    boolean possibleDrawTwo(Player p){
        int drawTwoCounter = 0;
        for(int i = 0; i < azzanCardArray.length; i++){
            if(p.getCharacterName() == "azzan"){
                if(azzanCardArray[i].isDisable()){
                    drawTwoCounter++;
                    if(drawTwoCounter == 7){
                        return true;
                    }
                }
            }else if(p.getCharacterName() == "lia"){
                if(liaCardArray[i].isDisable()){
                    drawTwoCounter++;
                    if(drawTwoCounter == 7){
                        return true;
                    }
                }
            }else if(p.getCharacterName() == "sutha"){
                if(suthaCardArray[i].isDisable()){
                    drawTwoCounter++;
                    if(drawTwoCounter == 7){
                        return true;
                    }
                }
            }else if(p.getCharacterName() == "oriax"){
                if(oriaxCardArray[i].isDisable()){
                    drawTwoCounter++;
                    if(drawTwoCounter == 7){
                        return true;
                    }
                }
            }
        }
        return false;
    }

    void drawTwoCards(Player p){
        if(p.getCharacterName() == "azzan") {
            String[] iconsArray = p.getAzzanDrawCards().getCardIcons().split(" ");
            String name = p.getAzzanCurrentCard(p.getAzzanCardNumber() - 1).getCardName();
            azzanCard0.setId("azzanCard" + (p.getAzzanCardNumber() - 1));
            azzanCard1Name.setText(name);
            azzanAttack0.setText(iconsArray[0]);
            azzanShield0.setText(iconsArray[1]);
            azzanAction0.setText(iconsArray[2]);
            azzanDraw0.setText(iconsArray[3]);
            azzanHeal0.setText(iconsArray[4]);
            azzanCardArray[0].setVisible(true);
            FadeTransition fade = new FadeTransition();
            fade.setDuration(Duration.millis(500));
            fade.setFromValue(0);
            fade.setToValue(1);
            fade.setNode(azzanCardArray[0]);
            fade.play();
            azzanCardArray[0].setDisable(false);

            iconsArray = p.getAzzanDrawCards().getCardIcons().split(" ");
            name = p.getAzzanCurrentCard(p.getAzzanCardNumber() - 1).getCardName();
            azzanCard1.setId("azzanCard" + (p.getAzzanCardNumber() - 1));
            azzanCard2Name.setText(name);
            azzanAttack1.setText(iconsArray[0]);
            azzanShield1.setText(iconsArray[1]);
            azzanAction1.setText(iconsArray[2]);
            azzanDraw1.setText(iconsArray[3]);
            azzanHeal1.setText(iconsArray[4]);
            azzanCardArray[1].setVisible(true);
            fade = new FadeTransition();
            fade.setDuration(Duration.millis(500));
            fade.setFromValue(0);
            fade.setToValue(1);
            fade.setNode(azzanCardArray[1]);
            fade.play();
            azzanCardArray[1].setDisable(false);
        }else if(p.getCharacterName() == "lia"){
            String[] iconsArray = p.getLiaDrawCards().getCardIcons().split(" ");
            String name = p.getLiaCurrentCard(p.getLiaCardNumber() - 1).getCardName();
            liaCard0.setId("liaCard" + (p.getLiaCardNumber() - 1));
            liaCard1Name.setText(name);
            liaAttack0.setText(iconsArray[0]);
            liaShield0.setText(iconsArray[1]);
            liaAction0.setText(iconsArray[2]);
            liaDraw0.setText(iconsArray[3]);
            liaHeal0.setText(iconsArray[4]);
            liaCardArray[0].setVisible(true);
            FadeTransition fade = new FadeTransition();
            fade.setDuration(Duration.millis(500));
            fade.setFromValue(0);
            fade.setToValue(1);
            fade.setNode(liaCardArray[0]);
            fade.play();
            liaCardArray[0].setDisable(false);

            iconsArray = p.getLiaDrawCards().getCardIcons().split(" ");
            name = p.getLiaCurrentCard(p.getLiaCardNumber() - 1).getCardName();
            liaCard1.setId("liaCard" + (p.getLiaCardNumber() - 1));
            liaCard2Name.setText(name);
            liaAttack1.setText(iconsArray[0]);
            liaShield1.setText(iconsArray[1]);
            liaAction1.setText(iconsArray[2]);
            liaDraw1.setText(iconsArray[3]);
            liaHeal1.setText(iconsArray[4]);
            liaCardArray[1].setVisible(true);
            fade = new FadeTransition();
            fade.setDuration(Duration.millis(500));
            fade.setFromValue(0);
            fade.setToValue(1);
            fade.setNode(liaCardArray[1]);
            fade.play();
            liaCardArray[1].setDisable(false);
        }
    }

    void draw(Player p) {
        p.showAllCards(p);
        if (currentCardDraw > 0) {
            if (p.getCharacterName() == "azzan") {
                //run through current draw card loop
                //call getAzzanDrawCards, returns card icons for that specific card
                //loop through azzanCardArray and find the first disabled card
                //once card is found, update all of its labels
                for (int i = 0; i < currentCardDraw; i++) {
                    String[] iconsArray = p.getAzzanDrawCards().getCardIcons().split(" ");
                    String name = p.getAzzanCurrentCard(p.getAzzanCardNumber() - 1).getCardName();
                    for (int j = 0; j < azzanCardArray.length; j++) {
                        if (azzanCardArray[j].isDisable()) {
                            if (j == 0) {
                                //System.out.println("in 0" + " " + i);
                                azzanCard0.setId("azzanCard" + (p.getAzzanCardNumber() - 1));
                                azzanCard1Name.setText(name);
                                azzanAttack0.setText(iconsArray[0]);
                                azzanShield0.setText(iconsArray[1]);
                                azzanAction0.setText(iconsArray[2]);
                                azzanDraw0.setText(iconsArray[3]);
                                azzanHeal0.setText(iconsArray[4]);
                                azzanCardArray[j].setVisible(true);
                            } else if (j == 1) {
                                //System.out.println("in 1" + " " + i);
                                azzanCard1.setId("azzanCard" + (p.getAzzanCardNumber() - 1));
                                azzanCard2Name.setText(name);
                                azzanAttack1.setText(iconsArray[0]);
                                azzanShield1.setText(iconsArray[1]);
                                azzanAction1.setText(iconsArray[2]);
                                azzanDraw1.setText(iconsArray[3]);
                                azzanHeal1.setText(iconsArray[4]);
                                azzanCardArray[j].setVisible(true);
                            } else if (j == 2) {
                                //System.out.println("in 2" + " " + i);
                                azzanCard2.setId("azzanCard" + (p.getAzzanCardNumber() - 1));
                                azzanCard3Name.setText(name);
                                azzanAttack2.setText(iconsArray[0]);
                                azzanShield2.setText(iconsArray[1]);
                                azzanAction2.setText(iconsArray[2]);
                                azzanDraw2.setText(iconsArray[3]);
                                azzanHeal2.setText(iconsArray[4]);
                                azzanCardArray[j].setVisible(true);
                            } else if (j == 3) {
                                //System.out.println("in 3" + " " + i);
                                azzanCard3.setId("azzanCard" + (p.getAzzanCardNumber() - 1));
                                azzanCard4Name.setText(name);
                                azzanAttack3.setText(iconsArray[0]);
                                azzanShield3.setText(iconsArray[1]);
                                azzanAction3.setText(iconsArray[2]);
                                azzanDraw3.setText(iconsArray[3]);
                                azzanHeal3.setText(iconsArray[4]);
                                azzanCardArray[j].setVisible(true);
                            } else if (j == 4) {
                                //System.out.println("in 4" + " " + i);
                                azzanCard4.setId("azzanCard" + (p.getAzzanCardNumber() - 1));
                                azzanCard5Name.setText(name);
                                azzanAttack4.setText(iconsArray[0]);
                                azzanShield4.setText(iconsArray[1]);
                                azzanAction4.setText(iconsArray[2]);
                                azzanDraw4.setText(iconsArray[3]);
                                azzanHeal4.setText(iconsArray[4]);
                                azzanCardArray[j].setVisible(true);
                            } else if (j == 5) {
                                azzanCard5.setId("azzanCard" + (p.getAzzanCardNumber() - 1));
                                azzanCard6Name.setText(name);
                                azzanAttack5.setText(iconsArray[0]);
                                azzanShield5.setText(iconsArray[1]);
                                azzanAction5.setText(iconsArray[2]);
                                azzanDraw5.setText(iconsArray[3]);
                                azzanHeal5.setText(iconsArray[4]);
                                azzanCardArray[j].setVisible(true);
                            } else if (j == 6) {
                                azzanCard6.setId("azzanCard" + (p.getAzzanCardNumber() - 1));
                                azzanCard7Name.setText(name);
                                azzanAttack6.setText(iconsArray[0]);
                                azzanShield6.setText(iconsArray[1]);
                                azzanAction6.setText(iconsArray[2]);
                                azzanDraw6.setText(iconsArray[3]);
                                azzanHeal6.setText(iconsArray[4]);
                                azzanCardArray[j].setVisible(true);
                            }
                            FadeTransition fade = new FadeTransition();
                            fade.setDuration(Duration.millis(500));
                            fade.setFromValue(0);
                            fade.setToValue(1);
                            fade.setNode(azzanCardArray[j]);
                            fade.play();
                            azzanCardArray[j].setDisable(false);

                            break;
                        }
                    }
                }
            } else if (p.getCharacterName() == "lia") {
                System.out.println("in lia");
                for (int i = 0; i < currentCardDraw; i++) {
                    String[] iconsArray = p.getLiaDrawCards().getCardIcons().split(" ");
                    String name = p.getLiaCurrentCard(p.getLiaCardNumber() - 1).getCardName();
                    for (int j = 0; j < liaCardArray.length; j++) {
                        if (liaCardArray[j].isDisable()) {
                            if (j == 0) {
                                //System.out.println("in 0" + " " + i);
                                liaCard0.setId("liaCard" + (p.getLiaCardNumber() - 1));
                                liaCard1Name.setText(name);
                                liaAttack0.setText(iconsArray[0]);
                                liaShield0.setText(iconsArray[1]);
                                liaAction0.setText(iconsArray[2]);
                                liaDraw0.setText(iconsArray[3]);
                                liaHeal0.setText(iconsArray[4]);
                                liaCardArray[j].setVisible(true);
                            } else if (j == 1) {
                                System.out.println("in 1" + " " + i);
                                liaCard1.setId("liaCard" + (p.getLiaCardNumber() - 1));
                                liaCard2Name.setText(name);
                                liaAttack1.setText(iconsArray[0]);
                                liaShield1.setText(iconsArray[1]);
                                liaAction1.setText(iconsArray[2]);
                                liaDraw1.setText(iconsArray[3]);
                                liaHeal1.setText(iconsArray[4]);
                                liaCardArray[j].setVisible(true);
                            } else if (j == 2) {
                                System.out.println("in 2" + " " + i);
                                liaCard2.setId("liaCard" + (p.getLiaCardNumber() - 1));
                                liaCard3Name.setText(name);
                                liaAttack2.setText(iconsArray[0]);
                                liaShield2.setText(iconsArray[1]);
                                liaAction2.setText(iconsArray[2]);
                                liaDraw2.setText(iconsArray[3]);
                                liaHeal2.setText(iconsArray[4]);
                                liaCardArray[j].setVisible(true);
                            } else if (j == 3) {
                                System.out.println("in 3" + " " + i);
                                liaCard3.setId("liaCard" + (p.getLiaCardNumber() - 1));
                                liaCard4Name.setText(name);
                                liaAttack3.setText(iconsArray[0]);
                                liaShield3.setText(iconsArray[1]);
                                liaAction3.setText(iconsArray[2]);
                                liaDraw3.setText(iconsArray[3]);
                                liaHeal3.setText(iconsArray[4]);
                                liaCardArray[j].setVisible(true);
                            } else if (j == 4) {
                                System.out.println("in 4" + " " + i);
                                liaCard4.setId("liaCard" + (p.getLiaCardNumber() - 1));
                                liaCard5Name.setText(name);
                                liaAttack4.setText(iconsArray[0]);
                                liaShield4.setText(iconsArray[1]);
                                liaAction4.setText(iconsArray[2]);
                                liaDraw4.setText(iconsArray[3]);
                                liaHeal4.setText(iconsArray[4]);
                                liaCardArray[j].setVisible(true);
                            } else if (j == 5) {
                                liaCard5.setId("liaCard" + (p.getLiaCardNumber() - 1));
                                liaCard6Name.setText(name);
                                liaAttack5.setText(iconsArray[0]);
                                liaShield5.setText(iconsArray[1]);
                                liaAction5.setText(iconsArray[2]);
                                liaDraw5.setText(iconsArray[3]);
                                liaHeal5.setText(iconsArray[4]);
                                liaCardArray[j].setVisible(true);
                            } else if (j == 6) {
                                liaCard6.setId("liaCard" + (p.getLiaCardNumber() - 1));
                                liaCard7Name.setText(name);
                                liaAttack6.setText(iconsArray[0]);
                                liaShield6.setText(iconsArray[1]);
                                liaAction6.setText(iconsArray[2]);
                                liaDraw6.setText(iconsArray[3]);
                                liaHeal6.setText(iconsArray[4]);
                                liaCardArray[j].setVisible(true);
                            }
                            FadeTransition fade = new FadeTransition();
                            fade.setDuration(Duration.millis(500));
                            fade.setFromValue(0);
                            fade.setToValue(1);
                            fade.setNode(liaCardArray[j]);
                            fade.play();
                            liaCardArray[j].setDisable(false);

                            break;
                        }
                    }
                }
            }
        }
    }


    void shields(Player p){
        if(currentCardShields > 0){
            if(p.getCharacterName() == "sutha"){
                suthaShieldLabel.setText(String.valueOf(Integer.parseInt(suthaShieldLabel.getText()) + currentCardShields));
            }else if(p.getCharacterName() == "azzan"){
                azzanShieldLabel.setText(String.valueOf(Integer.parseInt(azzanShieldLabel.getText()) + currentCardShields));
            }else if(p.getCharacterName() == "lia"){
                liaShieldLabel.setText(String.valueOf(Integer.parseInt(liaShieldLabel.getText()) + currentCardShields));
            }else if(p.getCharacterName() == "oriax"){
                oriaxShieldLabel.setText(String.valueOf(Integer.parseInt(oriaxShieldLabel.getText()) + currentCardShields));
            }
        }
    }

    void actions(Player p, int turn) {
        if (p.getCharacterName() == "lia") {
            int actionsCounter = Integer.parseInt(liaActionsLabel.getText());
            actionsCounter += currentCardActions;
            //lose an action because this is button clicked
            actionsCounter--;
            //add the new actions if available
            if (actionsCounter != 0) {
                liaActionsLabel.setText(String.valueOf(actionsCounter));
                for (int i = 0; i < liaCardArray.length; i++) {
                    if (liaCardArray[i].getId() ==  id) {
                        FadeTransition fade = new FadeTransition();
                        fade.setDuration(Duration.millis(500));
                        fade.setFromValue(1);
                        fade.setToValue(0);
                        fade.setNode(liaCardArray[i]);
                        fade.play();
                        liaCardArray[i].setDisable(true);
                    }
                }
            } else {
                liaActionsLabel.setText("0");
                playerTurnLabel.setText("Player " + turn + "'s Turn!");
                for (int i = 0; i < liaCardArray.length; i++) {
                    if (liaCardArray[i].getId() == id) {
                        FadeTransition fade = new FadeTransition();
                        fade.setDuration(Duration.millis(500));
                        fade.setFromValue(1);
                        fade.setToValue(0);
                        fade.setNode(liaCardArray[i]);
                        fade.play();
                        liaCardArray[i].setDisable(true);
                    }
                }

                if (turn-1 == 1) {
                    togglePlayerPanes(player2.getCharacterName());
                    playerTurn = 3;
                }else if (turn-1 == 2) {
                    togglePlayerPanes(player3.getCharacterName());
                    playerTurn = 4;
                }else if (turn-1 == 3) {
                    togglePlayerPanes(player4.getCharacterName());
                    playerTurn = 1;
                }else if(turn-1 == 0){
                    togglePlayerPanes(player1.getCharacterName());
                    playerTurn = 2;
                }

                liaActionsLabel.setText("1");
            }
        } else if (p.getCharacterName() == "azzan") {
            int actionsCounter = Integer.parseInt(azzanActionsLabel.getText());
            actionsCounter += currentCardActions;
            //lose an action because this is button clicked
            actionsCounter--;
            //add the new actions if available
            if (actionsCounter != 0) {
                azzanActionsLabel.setText(String.valueOf(actionsCounter));
                for (int i = 0; i < azzanCardArray.length; i++) {
                    if (azzanCardArray[i].getId() == id) {
                        FadeTransition fade = new FadeTransition();
                        fade.setDuration(Duration.millis(500));
                        fade.setFromValue(1);
                        fade.setToValue(0);
                        fade.setNode(azzanCardArray[i]);
                        fade.play();
                        azzanCardArray[i].setDisable(true);
                    }
                }

            } else {
                azzanActionsLabel.setText("0");
                playerTurnLabel.setText("Player " + turn + "'s Turn!");
                for (int i = 0; i < azzanCardArray.length; i++) {
                    if (azzanCardArray[i].getId() == id) {
                        FadeTransition fade = new FadeTransition();
                        fade.setDuration(Duration.millis(500));
                        fade.setFromValue(1);
                        fade.setToValue(0);
                        fade.setNode(azzanCardArray[i]);
                        fade.play();
                        azzanCardArray[i].setDisable(true);
                    }
                }
                if (turn - 1 == 1) {
                    togglePlayerPanes(player2.getCharacterName());
                    playerTurn = 3;
                } else if (turn - 1 == 2) {
                    togglePlayerPanes(player3.getCharacterName());
                    playerTurn = 4;
                }else if (turn-1 == 3) {
                    togglePlayerPanes(player4.getCharacterName());
                    playerTurn = 1;
                }else if(turn-1 == 0){
                    togglePlayerPanes(player1.getCharacterName());
                    playerTurn = 2;
                }
                azzanActionsLabel.setText("1");
            }
        }else if(p.getCharacterName() == "sutha"){
            int actionsCounter = Integer.parseInt(suthaActionsLabel.getText());
            actionsCounter += currentCardActions;
            //lose an action because this is button clicked
            actionsCounter--;
            //add the new actions if available
            if (actionsCounter != 0) {
                suthaActionsLabel.setText(String.valueOf(actionsCounter));
                for (int i = 0; i < suthaCardArray.length; i++) {
                    if (suthaCardArray[i].getId() == id) {
                        suthaCardArray[i].setVisible(false);
                    }
                }
            } else {
                suthaActionsLabel.setText("0");
                playerTurnLabel.setText("Player " + turn + "'s Turn!");
                for (int i = 0; i < suthaCardArray.length; i++) {
                    if (suthaCardArray[i].getId() == id) {
                        suthaCardArray[i].setVisible(false);
                    }
                }
                if (turn-1 == 1) {
                    togglePlayerPanes(player2.getCharacterName());
                    playerTurn = 3;
                }else if (turn-1 == 2) {
                    togglePlayerPanes(player3.getCharacterName());
                    playerTurn = 4;
                }else if (turn-1 == 3) {
                    togglePlayerPanes(player4.getCharacterName());
                    playerTurn = 1;
                }else if(turn-1 == 0){
                    togglePlayerPanes(player1.getCharacterName());
                    playerTurn = 2;
                }

                suthaActionsLabel.setText("1");
            }
        }else if(p.getCharacterName() == "oriax"){
            int actionsCounter = Integer.parseInt(oriaxActionsLabel.getText());
            actionsCounter += currentCardActions;
            //lose an action because this is button clicked
            actionsCounter--;
            //add the new actions if available
            if (actionsCounter != 0) {
                oriaxActionsLabel.setText(String.valueOf(actionsCounter));
                for (int i = 0; i < oriaxCardArray.length; i++) {
                    if (oriaxCardArray[i].getId() == id){
                        oriaxCardArray[i].setVisible(false);
                    }
                }
            } else {
                oriaxActionsLabel.setText("0");
                playerTurnLabel.setText("Player " + turn + "'s Turn!");
                for (int i = 0; i < oriaxCardArray.length; i++) {
                    if (oriaxCardArray[i].getId() == id) {
                        oriaxCardArray[i].setVisible(false);
                    }
                }
                if (turn-1 == 1) {
                    togglePlayerPanes(player2.getCharacterName());
                    playerTurn = 3;
                }else if (turn-1 == 2) {
                    togglePlayerPanes(player3.getCharacterName());
                    playerTurn = 4;
                }else if (turn-1 == 3) {
                    togglePlayerPanes(player4.getCharacterName());
                    playerTurn = 1;
                }else if(turn-1 == 0){
                    togglePlayerPanes(player1.getCharacterName());
                    playerTurn = 2;
                }

                oriaxActionsLabel.setText("1");
            }
        }
    }

    @FXML
    void attackPlayer(Event e){
        Button btn = (Button)e.getTarget();
        String getId = btn.getId();
        if(getId == "attackSutha"){
            if(currentCardAttack >= Integer.parseInt(suthaShieldLabel.getText())) {
                int newAttack = currentCardAttack - Integer.parseInt(suthaShieldLabel.getText());
                suthaShieldLabel.setText("0");
                suthaHpLabel.setText(String.valueOf(Integer.parseInt(suthaHpLabel.getText()) - newAttack));
            }else{
                int newAttack = Integer.parseInt(suthaShieldLabel.getText()) - currentCardAttack;
                suthaShieldLabel.setText(String.valueOf(newAttack));
            }
            attackBox.setDisable(true);
        }else if(getId == "attackAzzan"){
            if(currentCardAttack >= Integer.parseInt(azzanShieldLabel.getText())) {
                int newAttack = currentCardAttack - Integer.parseInt(azzanShieldLabel.getText());
                azzanShieldLabel.setText("0");
                azzanHpLabel.setText(String.valueOf(Integer.parseInt(azzanHpLabel.getText()) - newAttack));
            }else{
                int newAttack = Integer.parseInt(azzanShieldLabel.getText()) - currentCardAttack;
                azzanShieldLabel.setText(String.valueOf(newAttack));
            }
            attackBox.setDisable(true);
        }else if(getId == "attackLia"){
            if(currentCardAttack >= Integer.parseInt(liaShieldLabel.getText())) {
                int newAttack = currentCardAttack - Integer.parseInt(liaShieldLabel.getText());
                liaShieldLabel.setText("0");
                liaHpLabel.setText(String.valueOf(Integer.parseInt(liaHpLabel.getText()) - newAttack));
            }else{
                int newAttack = Integer.parseInt(liaShieldLabel.getText()) - currentCardAttack;
                liaShieldLabel.setText(String.valueOf(newAttack));
            }
            attackBox.setDisable(true);
        }else if(getId == "attackOriax"){
            if(currentCardAttack >= Integer.parseInt(oriaxShieldLabel.getText())) {
                int newAttack = currentCardAttack - Integer.parseInt(oriaxShieldLabel.getText());
                oriaxShieldLabel.setText("0");
                oriaxHpLabel.setText(String.valueOf(Integer.parseInt(oriaxHpLabel.getText()) - newAttack));
            }else{
                int newAttack = Integer.parseInt(oriaxShieldLabel.getText()) - currentCardAttack;
                oriaxShieldLabel.setText(String.valueOf(newAttack));
            }
            attackBox.setDisable(true);
        }
    }

    //disables all player panes except current player
    public void togglePlayerPanes(String character){
        if(character == "lia"){
            playerPane1.setDisable(true);
            playerPane2.setDisable(false);
            playerPane3.setDisable(true);
            playerPane4.setDisable(true);
        }else if(character == "oriax"){
            playerPane1.setDisable(true);
            playerPane2.setDisable(true);
            playerPane3.setDisable(false);
            playerPane4.setDisable(true);
        }else if(character == "azzan"){
            playerPane1.setDisable(false);
            playerPane2.setDisable(true);
            playerPane3.setDisable(true);
            playerPane4.setDisable(true);
        }else if(character == "sutha") {
            playerPane1.setDisable(true);
            playerPane2.setDisable(true);
            playerPane3.setDisable(true);
            playerPane4.setDisable(false);
        }
    }

    //players in game
    Player player1;
    Player player2;
    Player player3;
    Player player4;

    //controls player turns
    int playerTurn = 1;

    //takes in current selected card stats
    int currentCardAttack = 0;
    int currentCardShields = 0;
    int currentCardActions = 0;
    int currentCardHeal = 0;
    int currentCardDraw = 0;


    String[] characters;
    Player[] playerArray;
    boolean cardSelected = false;
    String id;


    //card arrays to match current selected card
    VBox[] liaCardArray;
    VBox[] oriaxCardArray;
    VBox[] azzanCardArray;
    VBox[] suthaCardArray;

    //selecting a card
    int toggle = 0;
    VBox previouslySelectedVbox;
    VBox vbox = new VBox();
    Effect shadow = new DropShadow();
    String currentSelectedCardStats;


}
