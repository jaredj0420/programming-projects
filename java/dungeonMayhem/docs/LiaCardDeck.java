package com.example.dungeon_mayhem;

public class LiaCardDeck {
    LiaCardDeck(){
        cards = new liaPaladin[28];
        for(int i = 0; i < cards.length; i++){
            liaPaladin card = new liaPaladin();
            cards[i] = card;
        }

        currentCard = 3;

    }

    void loadDeck(){
        for(int i = 0; i < cards.length; i++){
            cards[i].setCardName();
            cards[i].setCardStats();
            cards[i].setCardIcons();
        }
    }

    public liaPaladin[] getFirstCards(){
        return new liaPaladin[]{cards[0], cards[1], cards[2]};

    }

    public String getCharacterName(){
        return characterName;
    }

    public liaPaladin drawFromDeck(){
        return cards[currentCard++];
    }

    public liaPaladin getCard(int id){
        return cards[id];
    }

    public void showDeck(){
        for(int i = 0; i < cards.length; i++){
            System.out.println(i + " " + cards[i].getCardName() + " " + cards[i].getCardIcons());
        }
    }



    liaPaladin[] cards;
    String characterName = "lia";
    int currentCard;
}
