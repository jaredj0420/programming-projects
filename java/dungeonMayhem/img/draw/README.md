### Draw Method
- #### Functionality
    - every card except the starting 3 cards are disabled
    - finds the first disabled card in the card array with a loop
    - uses that spot to load in the new card and update its JavaFx id
- #### Screenshot
![draw](./draw_method.png)

