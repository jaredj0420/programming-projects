### Get Selected Method
- #### Functionality
    - gets the id of the card
    - id gets passed into a method to find that card
    - retrieves the card stats 
- #### Screenshot
![getSelected](./getSelected_method.png)

