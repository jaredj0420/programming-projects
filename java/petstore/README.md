# Petstore w/ MySql

### Project Summary
- Create a database with MySql and incorporate it into JSP. The petstore JSP is able to add, delete, and modify users in the page. 

### Project Details
- 4 month timeframe
- Solo
- Use of JSP and MySql
- MySql ERD

### Files w/ Code
- [Petstore Database](docs/petstoreDB.sql)
- [Customers](docs/customers.jsp)
- [Customer Form](docs/customerform.jsp)
- [Confirmation](docs/confirm.jsp)
- [Modify data](docs/modify.jsp)

### Screenshots of Web App
### ADD functionality
| JSP Database w/o modification | JSP Database Valid Info |
| ------- | ----- |
| ![dbNoChange](../img/petstore/modified.png) | ![dbChange](../img/petstore/valid.png) |

### ADD functionality cont.
| JSP Database Confirmation | JSP Database Update |
| ------- | ------ |
| ![dbConfirm](../img/petstore/passed.png) | ![dbUpdate](../img/petstore/display.png) |

### Edit/Delete shown in terminal run (ID 1)
![edit_delete](../img/petstore/changes.png)

Unmodified Database run: Shows database before any changes. It has 11 entries.

Second Database run: Shows edited database on first entry(Holly --> test). Remains at 11 entries.

Third Database run: Shows deleted first entry. Name test's data is deleted and Wendy takes the spot. Changes to 10 entries.